# Cloning project 1:
This is a single page "cloning project" which practices HTML and CSS by recreating same layout as exempled screenshot. 
Screenshot: https://res.cloudinary.com/gk3000/image/upload/v1572367458/single-page-workshop_barcelona-code-school.jpg

* Navigation on top with links to different section of the same webpage
* Header section with image background and Title, Subtitle and Call to action element on top of it
* Contact us section with embedded Google Map
* Responsive image gallery with images resizing/reorganizing based on the width of the screen
* Brands section which is a responsive grid of images with text below each one
* Team section with portraits and captions for the team members
* Footer section
